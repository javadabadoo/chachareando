package doo.daba.java.persistencia.criterio;

/**
 * Created with IntelliJ IDEA.
 * User: Gerardo Aquino
 * Date: 22/05/13
 * Time: 04:03 PM
 */
public interface CriterioConsulta {

    void agregaCriterio(Criterio criterio);

    void quitaCriterio(Criterio criterio);

}
