package doo.daba.java.persistencia;

import doo.daba.java.beans.EntradaBean;
import doo.daba.java.persistencia.DaoInterface;

/**
 * Created with IntelliJ IDEA.
 * User: Gerardo Aquino
 * Date: 16/05/13
 * Time: 04:53 PM
 */
public interface EntradaInterfaceDao extends DaoInterface<EntradaBean> {
}
